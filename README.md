# Core

Photon Core is a "Decoupled" Drupal REST distro that sits between your Frontend and  Universal Query - UQL. It processes user data into JSON, which can be consumed by any frontend: Minds and Pro, Drupal, pure Angular, React, Flutter as examples.